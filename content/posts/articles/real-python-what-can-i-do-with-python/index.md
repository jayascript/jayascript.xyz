---
title: "What Can I Do With Python? | Real Python"
date: 2018-06-25
menu:
  sidebar:
    name: "What Can I Do With Python?"
    identifier: real-python-what-can-i-do-with-python
    parent: articles
    weight: 180625
---

You’ve done it: you’ve finished a course or finally made it to the end of a book that teaches you the basics of programming with Python. You’ve mastered lists, dictionaries, classes, and maybe even some object oriented concepts.

So… what next?

Python is a very versatile programming language, with a plethora of uses in a variety of different fields. If you’ve grasped the basics of Python and are itching to build something with the language, then it’s time to figure out what your next step should be.

In this article, we offer several different projects, resources, and tutorials that you can use to start building things with Python.

## Read

**Read it on [Real Python](https://realpython.com/what-can-i-do-with-python/)**
