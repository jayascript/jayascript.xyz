---
title: "Care to Share? An Analysis of Twitter Sentiment as a Result of Public Action"
date: 2021-01-27
menu:
  sidebar:
    name: "Care to Share?"
    identifier: care-to-share-analysis-twitter-sentiment-result-public-action
    parent: data-stories
    weight: 210127
---

Naomi Osaka is once again involved in a stir on social media.

This time, its because of apparent discrepancies between the treatment of "top" tennis players and "lower" tennis players in the lead-up to the 2021 Australian Open. COVID-19 restrictions in the country have forced a hard quarantine onto the scores of players arriving in Australia; and some---like Osaka---are faring better than others.

Osaka is among the many "top" players based in Adelaide. Her perks include a gym on the hotel grounds and the ability to have up to four support staff accompany her on the court. A social media post with her posing on the court with four others made the rounds, sparking outrage in some of her "lower-level" counterparts stationed in Melbourne, who weren't even allowed out of their tiny, rat-infested hotel rooms.

Understandably, this caused quite a bit of controversy. According to the [New York Post](https://nypost.com/2021/01/19/naomi-osaka-photo-fueling-revolt-in-australian-open-quarantine/), the advantages Osaka has are a well-known problem in the tennis industry. Top players often receive special treatment; though, as Taro Daniel puts it, this special treatment may not be undeserved.

Osaka has been climbing the ranks of the tennis world for years now. She was the champion of the 2019 Australian Open. Just a year earlier, in 2018, she won her first Grand Slam title at the US Open---against 23-time champion Serena Williams.

And this was the *first* time Osaka was caught up in a stir on social media.

## Detecting The Tides of Public Opinion

To be fair, the real cause of the hubbub online in 2018 was Williams' unprofessional altercation with the umpire. The Twitterverse reacted in real-time to the events, with overall sentiment shifting as the game wore on.

In particular, sentiment towards Williams seemed to trend on the negative side, seemingly due to her unprofessional conduct on the court. Conversely, Naomi Osaka appeared to garner mostly positive reactions, likely as a result of her first win.

I decided to try and quantify this observation by performing sentiment analysis on a dataset of tweets that mentioned these world-class players. How was public opinion affected by the actions of these stars?

### Downloading the dataset

The 2018 US Open Women's singles final occurred on September 8. When I first performed this analysis, that date had long come and gone. Unfortunately, the Twitter API only allows one to mine tweets within a certain restricted timeline. Fortunately, other tools are able to work with Twitter's publicly exposed search functionality to cull tweets from any specified timeframe.

The package that I used for this project is called [GetOldTweets-python](https://github.com/Jefferson-Henrique/GetOldTweets-python). While much more recent and computationally efficient solutions have been developed since (such as the [Twitter Intelligence Tool](https://github.com/twintproject/twint)), this is what I had to work with at the time.

I started by just querying the names of each player to see what kinds of results I would get. Even with only 20 tweets, I could already see a pattern forming. On the 13th of September 2018, tweets about Naomi Osaka were filled with words like *adorable*, *cute*, and *refreshing,* whereas tweets about Williams contained words like *shut up*, *lost*, and *racist.*

Thinking back on [the last project I did](#), I'd gathered way too much information. I collected nearly 100k tweets with all sorts of unnecessary identifying information---tweet ID, time stamp, geolocation, you name it. This time, I wanted to try and keep things pretty simple.

At the bare minimum, I would only need the tweet text. GetOldTweets automatically includes hashtags if they match the search query. I collected the tweet ID, tweet location, and tweet text in both English and Japanese for both queries (though we'll only be looking at the English results here).

I built the database interactively in a Jupyter notebook (which is a HORRIBLE idea, by the way!!) using the queries "serena williams" and "naomi osaka" to grab tweets from September 8, 2018 until the 9th. The English query database started with 30,000 tweets total, 15,000 for each query.

![Original, unprocessed database](001-original_db.png)

### Distilling the data

I realized that the `tweet_loc` and `tweet_id` columns were pretty useless; this tool couldn't glean geographic data, and `tweet_id` didn't provide enough salient info to justify keeping it around. So, I dropped both of these columns.

I also dropped any duplicated tweets, any observations that had no `tweet_text` at all, and any tweets in a language other than English. Then, I needed to prep the data for analysis. First, I followed the [KDNuggets Text Data Preprocessing Walkthrough for Python](https://www.kdnuggets.com/2018/03/text-data-preprocessing-walkthrough-python.html) to remove hyperlinks, hashtags, and mentions. Then, I used [DJ Sarkar's `contractions` module](https://github.com/dipanjanS/practical-machine-learning-with-python/tree/master/bonus%20content/nlp%20proven%20approach) to expand all contractions. Finally, I removed any special characters, converted all text to lower case, tokenized each tweet, removed stop words, and reduced the remaining words to their roots.

This process was *highly inefficient.* Luckily, there are better tools available nowadays that automate most of these steps.

![Processed database](002-processed_db.png)

You can view the code I wrote for this step over on GitLab: [Check out these snippets](https://gitlab.com/-/snippets/2067385)

### Discovering sentiment

I tested several different methods for analyzing the tweets, using:

* the original tweet itself
* the averaged sentiment of each token
* the overall sentiment of the concatenated tokens

Concatenating the tokens and analyzing the phrase as a whole appeared to give the starkest sentiment contrast, so I proceeded with this approach.

![Database with sentiment](003-sentiment.png)

I performed a crude analysis by summing and averaging the sentiments for both queries. For 11,380 tweets with the query "naomi osaka," the total sentiment gleaned was 1897.445, leading to an average sentiment of 0.16 in the positive direction.

![Database head showing positive sentiment](004-sentiment_positive.png)

In contrast, 12,395 tweets with the query "serena williams" only had a total sentiment score of 1305.5, with an average sentiment of 0.11 in the positive direction.

![Database tail showing negative sentiment](004-sentiment_negative.png)

Clearly, public opinion about Osaka was more favorable.

### Determining the reason

Why, though?

This is one of my favorite questions. We know that tweets about Serena Williams were more negative on the whole. But why, though? What were people talking about, exactly?

I decided to create word clouds for each search query to view the results by volume.

![Word cloud for tweets containing the query "naomi osaka"](NaomiCloud.png)

It's obvious there's a lot of positive sentiment towards Osaka. As there should be---she was declared the winner, after all! Some of the most frequent words are "congrats" and "congratulations." Others that are immediately visible include "deserve," "well-deserved," and "fan."

![Word cloud for tweets containing the query "serena williams"](SerenaCloud.png)

Of course, there are no congratulatory remarks in Williams' tweets. The sizes of her words are much smaller than Osaka's, suggesting a wider range of topics. My eyes are drawn to the words "cheat," "behavior," "never," "shame," "need," "class act," and "lose."

![Sentiment bar chart](005-sentiment_bar_chart.png)

Both players had an overwhelming number of positive tweets, but no doubt Williams had more negative tweets overall than Osaka; and Osaka clearly had more positive tweets than Williams.

### Discussing the results

This is no surprise. Williams conducted herself in a very unprofessional manner, so much so that she had to send a [letter of apology](https://people.com/sports/serena-williams-apology-letter-naomi-osaka/) to Osaka after the match. It's clear that public opinion seemed to sway in favor of the professionalism shown by Osaka, and away from the rude behavior on display by Williams.

It's obvious that our behavior has consequences, and nowhere is this more apparent than in the public sphere, where the actions and opinions of public figures can be reacted to in real-time thanks to the World Wide Web. As such, participants in the public forum would do well to conduct themselves accordingly, lest they become the subject of the next Twitter storm.

### Double-checking the claims

But how can we really be sure that human action is the cause of negative sentiment? Well, one tool we can use is **change detection.** This technique will allow us to calculate sentiment over a period of time, and detect if the overall sentiment changes in any significant way during that time period.

We've seen so far that Naomi Osaka tends to garner positive tweets. However, given her new controversy surrounding the 2021 Australian Open, Osaka has been lambasted in the Twitterverse for the "unequal treatment" she's received, and the benefits she's had while training in Adelaide. Unlike her previous action of winning the 2018 US Open, her current decision to tweet a picture of herself on the court with four support staff seems to have caused quite the flurry of negative sentiment.

{{< tweet 1350410710300454916 >}}

If we can detect a significant change in sentiment around the time of this event, especially one that's a significant change from what we saw in her 2018 US Open tweets, then we can reasonably conclude that her actions had a salient effect on public opinion on Twitter.

I decided to scrape a few recent tweets to find out. This time, I used [Twint](https://github.com/twintproject/twint) to speed up the process.

```bash
$ twint -s "naomi osaka" --since 2021-01-16 -o naomi.csv --csv
```

This pulled 4,391 tweets into a CSV file (within a matter of seconds, might I add).

The average sentiment for Osaka since the 16th up until the time of this writing was still pretty high: at 0.159 in the positive direction. However, using change detection, one notices a sharp decline in average sentiment hour-over-hour right around the time when the "controversial" picture was posted:

![Change detection results](006-change_detected.png)

The lowest sentiment she received was during the hour of 9PM EST on the 16th, with an average score of -0.572 in the negative direction. This is a considerable drop from her overall average.

## Conclusion

We know that our actions have consequences, but for some of us those consequences may be a bit more salient than others. In 2018, Osaka got to see firsthand how unprofessional behavior could lead to a storm of public opinion. Now, she's experiencing this herself as people from around the globe question her choices as the 2021 Open approaches.

I'm not here to comment on those choices, nor the opinions surrounding them. I'm merely interested in presenting what the data appear to say. However, I do believe that it has never been more dangerous for public figures to be in the public sphere. The immediate and global nature of platforms like Twitter can quickly cause public opinion to turn into an onslaught of sentiment---be it positive or negative.

As such, we can only be careful how, where, when---and with who---we choose to share ourselves.
